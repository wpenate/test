
import './App.css';
import Ejemplo from './views/Ejemplo/Index';
import {
  BrowserRouter as Router,
  Routes ,
  Route,
  Link
} from "react-router-dom";

function App() {

    return (
    <div className="App">
      <div className="container text-center">
        <div className="row">
          <div className="col">
            {/* Menu principal */}
            <Router>
              <div>
              <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="container-fluid">
                <a className="navbar-brand" href="/">HOLA LILIBETH </a>
                
                <div className="collapse navbar-collapse" id="navbarNav">

                  {/* EJEMPLO */}
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link className="nav-link " to="/">Ejemplo</Link>
                    </li>
                  </ul>

                  {/* PARA NUEVAS PANTALLAS */}
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link className="nav-link " to="/departamento">Departamento</Link>
                    </li>
                  </ul>
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link className="nav-link " to="/municipio">Municipio</Link>
                    </li>
                  </ul>
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link className="nav-link " to="/">Productos</Link>
                    </li>
                  </ul>
                  <ul className="navbar-nav">
                    <li className="nav-item">
                      <Link className="nav-link " to="/">Genero</Link>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
              {/* CAMBIO DE RUTAS DEL NAVEGADOR */}
                <Routes >
                    {/* RUTA EJEMPLO */}
                    <Route path='/' element={<Ejemplo />} />

                    {/* PARA NUEVAS RUTAS */}
                    <Route path='/departamento' element={<Ejemplo />} />
                    
                </Routes >
              </div>
            </Router>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
